//
//  FZUser.h
//  lemobile
//
//  Created by Aymeric Gallissot on 18/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FZUser : NSObject

// proprieties
@property (nonatomic, strong) NSString *fbid;
@property (nonatomic, strong) NSString *fbtoken;

@property (nonatomic, assign) BOOL connected;

+ (FZUser *)sharedUser;

// method

- (BOOL)signinWithFacebookID:(NSString *)fbid token:(NSString *)fbtoken;
- (void)signoff;

@end
