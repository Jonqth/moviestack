//
//  FZViewController.m
//  lemobile
//
//  Created by Aymeric Gallissot on 18/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZViewController.h"

@interface FZViewController ()

@end

@implementation FZViewController

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeSettings) name:@"closeSettings" object:nil];
    
    //
    UIImage *titleImage = [UIImage imageNamed:@"ms_titleview.png"];
    UIImageView *titleView = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width/2)-(titleImage.size.width/2), 15,
                                                                           titleImage.size.width, titleImage.size.height)];
    [titleView setImage:titleImage];
    [self.navigationController.navigationBar addSubview:titleView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (UIBarButtonItem *)buttonSettings{
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonSettings setImage:[UIImage imageNamed:@"ms_params.png"] forState:UIControlStateNormal];
    [buttonSettings setBackgroundColor:[UIColor clearColor]];
    [buttonSettings addTarget:self action:@selector(toggleSettings) forControlEvents:UIControlEventTouchUpInside];
    [buttonSettings setFrame:CGRectMake(0, 0, 41, 32)];
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 41, 31)];
    [buttonView addSubview:buttonSettings];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonView];
    
    return barButtonItem;
}

- (UIBarButtonItem *)buttonBack{
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonSettings setImage:[UIImage imageNamed:@"ms_back.png"] forState:UIControlStateNormal];
    [buttonSettings setBackgroundColor:[UIColor clearColor]];
    [buttonSettings addTarget:self action:@selector(actionBack) forControlEvents:UIControlEventTouchUpInside];
    [buttonSettings setFrame:CGRectMake(0, 0, 42, 32)];
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 42, 31)];
    [buttonView addSubview:buttonSettings];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonView];
    
    return barButtonItem;
}

- (UIBarButtonItem *)buttonAddWatchlist{
    UIButton *buttonSettings = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonSettings setImage:[UIImage imageNamed:@"ms_add.png"] forState:UIControlStateNormal];
    [buttonSettings setBackgroundColor:[UIColor clearColor]];
    [buttonSettings addTarget:self action:@selector(actionAddWatchlist) forControlEvents:UIControlEventTouchUpInside];
    [buttonSettings setFrame:CGRectMake(0, 0, 42, 32)];
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 42, 31)];
    [buttonView addSubview:buttonSettings];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonView];
    
    return barButtonItem;
}

- (void)toggleSettings{
    CGRect navFrame = self.navigationController.view.frame;
    
    if(navFrame.origin.y == 0){
        
        [UIView animateWithDuration:0.3 animations:^{
            CGRect navFrameNew = CGRectMake(navFrame.origin.x, 66.0, navFrame.size.width, navFrame.size.height);
            self.navigationController.view.frame = navFrameNew;
        } completion:^(BOOL finished) {
            self.settingsVisible = YES;
        }];
    }
    else{
        [UIView animateWithDuration:0.3 animations:^{
            CGRect navFrameNew = CGRectMake(navFrame.origin.x, 0.0, navFrame.size.width, navFrame.size.height);
            self.navigationController.view.frame = navFrameNew;
        } completion:^(BOOL finished){
            self.settingsVisible = NO;
        }];
    }
}

- (void)closeSettings{
    CGRect navFrame = self.navigationController.view.frame;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect navFrameNew = CGRectMake(navFrame.origin.x, 0.0, navFrame.size.width, navFrame.size.height);
        self.navigationController.view.frame = navFrameNew;
    } completion:^(BOOL finished){
        self.settingsVisible = NO;
    }];
}

- (void)actionBack{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)actionAddWatchlist{
    // To created in controller
}

@end
