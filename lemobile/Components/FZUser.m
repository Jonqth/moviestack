//
//  FZUser.m
//  lemobile
//
//  Created by Aymeric Gallissot on 18/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZUser.h"

#define CONNECTED @"FZUserConnected"
#define FBID @"FZUserFBID"
#define FBTOKEN @"FZUserFBToken"

@implementation FZUser

static FZUser *sharedUser = nil;

+ (FZUser *)sharedUser{
    
    static dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        sharedUser = [[FZUser alloc] init];
    });
    
    return sharedUser;
}

- (id)init{
    
	if ((self = [super init]) != nil){
        
        NSLog(@"FZUser: init");
        
        self.fbid = nil;
        self.fbtoken = nil;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
        // Connected
        if([defaults objectForKey:CONNECTED]) {
            if([[defaults objectForKey:CONNECTED] isEqualToString:@"YES"]){
                self.fbid = [defaults objectForKey:FBID];
                self.fbtoken = [defaults objectForKey:FBTOKEN];
                self.connected = YES;
            }
            else{
                self.connected = NO;
            }
        }
        else{
            [defaults setObject:@"NO" forKey:CONNECTED];
            [defaults synchronize];
            self.connected = NO;
        }
        
        // Notifications
        /*
        if([defaults objectForKey:DATA_NOTIFICATIONS]) {
            if([[defaults objectForKey:DATA_NOTIFICATIONS] isEqualToString:@"YES"]){
                self.notifications = YES;
            }
            else{
                self.notifications = NO;
            }
        }
        else{
            [defaults setObject:@"YES" forKey:DATA_NOTIFICATIONS];
            [defaults synchronize];
            self.notifications = YES;
        }
        */
        
    }
    
	return self;
}

- (BOOL)signinWithFacebookID:(NSString *)fbid token:(NSString *)fbtoken{
    self.connected = YES;
    self.fbid = fbid;
    self.fbtoken = fbtoken;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:@"YES" forKey:CONNECTED];
    [defaults setObject:fbid forKey:FBID];
    [defaults setObject:fbtoken forKey:FBTOKEN];
    [defaults synchronize];
    
    return YES;
}

- (void)signoff{
    self.connected = NO;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:@"NO" forKey:CONNECTED];
    [defaults removeObjectForKey:FBID];
    [defaults removeObjectForKey:FBTOKEN];
    [defaults synchronize];
}

@end
