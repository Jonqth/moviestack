//
//  FZBase64.h
//  lemobile
//
//  Created by Aymeric Gallissot on 19/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FZBase64 : NSObject

+ (NSData *)decodeBase64WithString:(NSString *)strBase64;

@end
