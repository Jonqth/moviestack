//
//  FZViewController.h
//  lemobile
//
//  Created by Aymeric Gallissot on 18/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@interface FZViewController : UIViewController

@property (nonatomic, assign) BOOL settingsVisible;

- (UIBarButtonItem *)buttonSettings;
- (UIBarButtonItem *)buttonBack;
- (UIBarButtonItem *)buttonAddWatchlist;

- (void)toggleSettings;
- (void)closeSettings;
- (void)actionAddWatchlist;
@end
