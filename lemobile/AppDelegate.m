//
//  AppDelegate.m
//  lemobile
//
//  Created by Aymeric Gallissot on 18/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "AppDelegate.h"

#import "FZUser.h"

#import "SigninViewController.h"
#import "WatchlistViewController.h"

// Moodstocks SDK
#import "MSAvailability.h"
#import "MSDebug.h"
#import "MSScanner.h"

// -------------------------------------------------
// Moodstocks API key/secret pair
// -------------------------------------------------
#define MS_API_KEY @"ffrliv1cvijmcmjuxnbu"
#define MS_API_SEC @"4RU8NP73uC5FvbYN"

@interface AppDelegate () <MSScannerDelegate>
- (void)scannerInit;
@end

@implementation AppDelegate
@synthesize isScannerAvailable;
@synthesize scannerOpenError;
@synthesize scannerSyncError;

@synthesize fontInfo;
@synthesize fontRate;
@synthesize fontTitle;
@synthesize colorInfo;
@synthesize colorTitle;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Moodstocks SDK setup
    [self scannerInit];
    [self scannerSync];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"ms_navbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Screen iPhone 5
    if([[UIScreen mainScreen]bounds].size.height == 568){
        self.mainScreeniPhone5 = YES;
    }
    else{
        self.mainScreeniPhone5 = NO;
    }
    
    // Stylesheet
    self.fontTitle = [UIFont fontWithName:@"Intro" size:15.5f];
    self.fontRate = [UIFont fontWithName:@"Intro" size:12.f];
    self.fontInfo = [UIFont fontWithName:@"Intro" size:10.2f];
    
    self.colorTitle = [UIColor colorWithRed:(18/255.f) green:(18/255.f) blue:(18/255.f) alpha:1];
    self.colorInfo = [UIColor colorWithRed:(254/255.f) green:(203/255.f) blue:(0/255.f) alpha:1];
    
        
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor colorWithRed:(30/255.f) green:(30/255.f) blue:(30/255.f) alpha:1];
    
    self.mainNav = nil;
    
    SigninViewController *signinView = [[SigninViewController alloc] init];
    self.mainNav = [[UINavigationController alloc] initWithRootViewController:signinView];
    
    
    // IF user connected
    if([FZUser sharedUser].connected){
        WatchlistViewController *watchlistView = [[WatchlistViewController alloc] init];
        [self.mainNav pushViewController:watchlistView animated:NO];
    }
    
    
    
    UIImageView *settingsBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 66)];
    [settingsBackground setImage:[UIImage imageNamed:@"ms_decoback.png"]];
    
    UIView *settingsView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, 320, 66)];
    [settingsView addSubview:settingsBackground];
    
    // Settings button signoff
    UIButton *buttonSignoff = [[UIButton alloc] initWithFrame:CGRectMake(40, 14, 240, 37)];
    [buttonSignoff setBackgroundImage:[UIImage imageNamed:@"ms_deco.png"] forState:UIControlStateNormal];
    [buttonSignoff setBackgroundColor:[UIColor redColor]];
    [buttonSignoff addTarget:self action:@selector(actionButtonSignoff) forControlEvents:UIControlEventTouchUpInside];
    [settingsView addSubview:buttonSignoff];
        
    
    [self.window addSubview:settingsView];
    
    
    [self.window setRootViewController:self.mainNav];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self scannerSync];
}

- (void)actionButtonSignoff{
    NSLog(@"Disconnected");
    
    [[FZUser sharedUser] signoff];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"closeSettings" object:nil];
    [self.mainNav popToRootViewControllerAnimated:YES];
}


#pragma mark - Moodstocks SDK

- (void)scannerInit {
    isScannerAvailable = NO;
    scannerOpenError = -1;
    scannerSyncError = -1; // no sync yet
    if (MSDeviceCompatibleWithSDK()) {
#if MS_SDK_REQUIREMENTS
        isScannerAvailable = YES;
        scannerOpenError = MS_SUCCESS;
        NSError *err;
        MSScanner *scanner = [MSScanner sharedInstance];
        if (![scanner openWithKey:MS_API_KEY secret:MS_API_SEC error:&err]) {
            scannerOpenError = [err code];
            // == DO NOT USE IN PRODUCTION: THIS IS A HELP MESSAGE FOR DEVELOPERS
            if (scannerOpenError == MS_CREDMISMATCH) {
                NSString *errStr = @"there is a problem with your key/secret pair: "
                "the current pair does NOT match with the one recorded within the on-disk datastore. "
                "This could happen if:\n"
                " * you have first built & run the app without replacing the default"
                " \"ApIkEy\" and \"ApIsEcReT\" pair, and later on replaced it with your real key/secret,\n"
                " * or, you have first made a typo on the key/secret pair, built & run the"
                " app, and later on fixed the typo and re-deployed.\n"
                "\n"
                "To solve your problem:\n"
                " 1) uninstall the app from your device,\n"
                " 2) make sure to properly configure your key/secret pair within MSScanner.m\n"
                " 3) re-build & run\n";
                MSDLog(@"\n\n [MOODSTOCKS SDK] SCANNER OPEN ERROR: %@", errStr);
                
                // NOTE: we purposely crash the app here so that the developer detects the problem
                [[NSException exceptionWithName:@"MSScannerException"
                                         reason:@"Credentials mismatch"
                                       userInfo:nil] raise];
            }
            // == DO NOT USE IN PRODUCTION: THIS IS A HELP MESSAGE FOR DEVELOPERS
            else {
                MSDLog(@" [MOODSTOCKS SDK] SCANNER OPEN ERROR: %@", MSErrMsg(scannerOpenError));
            }
        }
#endif
    }
}

- (void)scannerSync {
#if MS_SDK_REQUIREMENTS
    MSScanner *scanner = [MSScanner sharedInstance];
    if ([scanner isSyncing]) return;
    [scanner syncWithDelegate:self];
#endif
}

- (void)scannerDidSync:(MSScanner *)scanner {
    scannerSyncError = MS_SUCCESS;
    MSDLog(@" [MOODSTOCKS SDK] SYNC SUCCEEDED (%d IMAGE(S))", [scanner count:nil]);
}

- (void)scanner:(MSScanner *)scanner failedToSyncWithError:(NSError *)error {
    ms_errcode ecode = [error code];
    if (ecode == MS_BUSY) return;
    scannerSyncError = ecode;
    MSDLog(@" [MOODSTOCKS SDK] SYNC ERROR: %@", MSErrMsg(ecode));
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
