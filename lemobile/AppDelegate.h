//
//  AppDelegate.h
//  lemobile
//
//  Created by Aymeric Gallissot on 18/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//


#import <UIKit/UIKit.h>

#include "moodstocks_sdk.h" /* ms_errcode */

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *mainNav;
@property (assign, nonatomic) int mainScreeniPhone5;


// Moodstocks
@property (assign, readonly) BOOL isScannerAvailable;     // YES if the device is compatible, NO otherwise
@property (assign, readonly) ms_errcode scannerOpenError; // Moodstocks SDK open error
@property (assign, readonly) ms_errcode scannerSyncError; // Moodstocks SDK last sync error

// Stylesheet
@property (strong, nonatomic) UIFont *fontTitle;
@property (strong, nonatomic) UIColor *colorTitle;
@property (strong, nonatomic) UIFont *fontInfo;
@property (strong, nonatomic) UIColor *colorInfo;
@property (strong, nonatomic) UIFont *fontRate;
@property (strong, nonatomic) UIFont *details;


// Moodstocks SDK synchronization
- (void)scannerSync;
@end
