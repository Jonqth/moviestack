//
//  Movie.h
//  lemobile
//
//  Created by Aymeric on 19/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

@property (assign, nonatomic) int movie_id;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *image;
@property (strong, nonatomic) NSString *trailer;
@property (strong, nonatomic) NSString *genre;
@property (strong, nonatomic) NSString *releasedate;
@property (assign, nonatomic) int runtime;
@property (assign, nonatomic) float rating;

@end
