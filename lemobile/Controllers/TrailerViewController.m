//
//  TrailerViewController.m
//  lemobile
//
//  Created by Jonathan Araujo-Levy on 19/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "TrailerViewController.h"

@interface TrailerViewController (){

    NSString *MoviePath;
    
}
@end

@implementation TrailerViewController

- (id)initWithUrl:(NSString*)url {
    
    MoviePath = [bundle pathForResource:film ofType:leType];
    
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

        NSBundle *bundle=[NSBundle mainBundle];
        
        movie=[[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:moviePath]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finFilm:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
        
        
        [self.view addSubview:movie.view];
        
        
        //---play movie---
        MPMoviePlayerController *player= [movie moviePlayer];
        [player play];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
