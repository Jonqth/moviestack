//
//  SigninViewController.m
//  lemobile
//
//  Created by Aymeric Gallissot on 18/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "SigninViewController.h"

#import <Accounts/Accounts.h>

#import "FZUser.h"

#import "WatchlistViewController.h"

@interface SigninViewController ()

@property (strong, nonatomic) ACAccountStore *accountStore;
@property (strong, nonatomic) ACAccount *fbAccount;

@end

@implementation SigninViewController

@synthesize accountStore = _accountStore;
@synthesize fbAccount = _fbAccount;

- (id)init{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    UIImageView *signinBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 548)];
    [signinBackground setImage:[UIImage imageNamed:@"signin-bg@2x.png"]];
    
    [self.view addSubview:signinBackground];
    
    float FBButtonY = ([UIScreen mainScreen].bounds.size.height)-20-45-36;
        
    UIButton *FBButton = [[UIButton alloc] initWithFrame:CGRectMake(41, FBButtonY, 238, 45)];
    [FBButton setImage:[UIImage imageNamed:@"signin@2x.png"] forState:UIControlStateNormal];
    [FBButton addTarget:self action:@selector(actionFBButton) forControlEvents:UIControlEventTouchUpInside];
    [FBButton setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:FBButton];
    
    // Activity indicator
    self.activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(150, FBButtonY-40, 20, 20)];
    [self.activity setBackgroundColor:[UIColor clearColor]];
    [self.activity setHidesWhenStopped:YES];
    [self.activity setHidden:YES];
    
    // Add to cell
    [self.view addSubview:self.activity];
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// WARNING Aymeric: add UIActivity, need time for loading Facebook data
- (void)actionFBButton{
    
    [self.activity startAnimating];
    
    // Initialize the account store
    self.accountStore = [[ACAccountStore alloc] init];
    
    // Get the Facebook account type for the access request
    ACAccountType *fbAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSDictionary *options = @{
    @"ACFacebookAppIdKey" : @"507014066027567",
    @"ACFacebookPermissionsKey" : @[@"email"],
    @"ACFacebookAudienceKey" : ACFacebookAudienceFriends};
    
    // Request access to the Facebook account with the access info
    [self.accountStore requestAccessToAccountsWithType:fbAccountType options:options completion:^(BOOL granted, NSError *error) {
        if (granted) {
            NSArray *accounts = [self.accountStore accountsWithAccountType:fbAccountType];
            self.fbAccount = [accounts lastObject];
                                                    
            // Get the access token, could be used in other scenarios
            ACAccountCredential *fbCredential = [self.fbAccount credential];            
            NSString *fbtoken = [fbCredential oauthToken];
            NSString *fbid = [self.fbAccount valueForKeyPath:@"properties.uid"];
                                                    
            
            [[FZUser sharedUser] signinWithFacebookID:fbid token:fbtoken];
            
            WatchlistViewController *watchView = [[WatchlistViewController alloc] init];
            [self.navigationController pushViewController:watchView animated:YES];
            
            [self.activity stopAnimating];
                                                    
        } else {
            NSLog(@"Access not granted");
            NSLog(@"Error description: %@",error.description);
        }
    }];
}


@end
