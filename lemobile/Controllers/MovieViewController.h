//
//  MovieViewController.h
//  lemobile
//
//  Created by Aymeric on 19/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZViewController.h"

#import "Movie.h"
#import <MapKit/MapKit.h>

@interface MovieViewController : FZViewController <CLLocationManagerDelegate, MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) Movie *movie;
@property (strong, nonatomic) NSString *movie_base64;

- (id)initWithMovie:(Movie *)movie;
- (id)initWithMovieURLBase64:(NSString *)movieBase64;

@property (strong, nonatomic) CLLocationManager *MSLocationManager;
@property (strong, nonatomic) MKMapView *MSMapView;

@end
