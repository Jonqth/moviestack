//
//  WatchlistViewController.h
//  lemobile
//
//  Created by Aymeric Gallissot on 18/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZViewController.h"

#import <RestKit/RestKit.h>

#import <SDWebImage/UIImageView+WebCache.h>

@interface WatchlistViewController : FZViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@property (strong, nonatomic) NSArray *data;

@end
