//
//  ScannerViewController.m
//  lemobile
//
//  Created by Aymeric on 19/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "ScannerViewController.h"

#import "MovieViewController.h"

#import "MSDebug.h"
#import "MSImage.h"

#include "moodstocks_sdk.h"

static NSInteger kMSScanOptions = MS_RESULT_TYPE_IMAGE;

@interface ScannerViewController ()

@end

@implementation ScannerViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
        _scannerSession = [[MSScannerSession alloc] initWithScanner:[MSScanner sharedInstance]];
#if MS_SDK_REQUIREMENTS
        [_scannerSession setScanOptions:kMSScanOptions];
        [_scannerSession setDelegate:self];
#endif
        
    }
    return self;
}

- (void)dealloc {
    
    
    _result = nil;
    
}

#pragma mark - Private stuff


#pragma mark - View lifecycle

- (void)loadView {
    [super loadView];
    
    CGFloat w = self.view.frame.size.width;
    CGFloat h = self.view.frame.size.height;
    
    CGRect videoFrame = CGRectMake(0, 0, w, h);
    _videoPreview = [[UIView alloc] initWithFrame:videoFrame];
    _videoPreview.backgroundColor = [UIColor blackColor];
    _videoPreview.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _videoPreview.autoresizesSubviews = YES;
    [self.view addSubview:_videoPreview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = nil;
    
    CALayer *videoPreviewLayer = [_videoPreview layer];
    [videoPreviewLayer setMasksToBounds:YES];
    
    CALayer *captureLayer = [_scannerSession previewLayer];
    [captureLayer setFrame:[_videoPreview bounds]];
    
    [videoPreviewLayer insertSublayer:captureLayer below:[[videoPreviewLayer sublayers] objectAtIndex:0]];
    
    // Try again to synchronize if the last sync failed
    id<UIApplicationDelegate> appDelegate = [[UIApplication sharedApplication] delegate];
    if ([appDelegate respondsToSelector:@selector(scannerSyncError)] &&
        [appDelegate respondsToSelector:@selector(scannerSync)]) {
        //if ([appDelegate performSelector:@selector(scannerSyncError)] != MS_SUCCESS)
            //[appDelegate performSelector:@selector(scannerSync)];
    }
    
    [_scannerSession startCapture];
    
    /*
    NSDictionary *state = [NSDictionary dictionaryWithObjectsAndKeys:
                           [NSNumber numberWithBool:!!(kMSScanOptions & MS_RESULT_TYPE_EAN8)],   @"decode_ean_8",
                           [NSNumber numberWithBool:!!(kMSScanOptions & MS_RESULT_TYPE_EAN13)],  @"decode_ean_13",
                           [NSNumber numberWithBool:!!(kMSScanOptions & MS_RESULT_TYPE_QRCODE)], @"decode_qrcode",
                           [NSNumber numberWithBool:!!(kMSScanOptions & MS_RESULT_TYPE_DMTX)],   @"decode_datamatrix", nil];
     */
    
    UISwipeGestureRecognizer *swipeWatchlist = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(actionWatchlist)];
    swipeWatchlist.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeWatchlist];
    
    UISwipeGestureRecognizer *swipeMovie = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(actionMovie)];
    swipeMovie.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeMovie];
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self resume];
}



- (void)viewDidUnload {
    [super viewDidUnload];
    
    _videoPreview = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Actions

- (void)snapAction:(UIGestureRecognizer *)gestureRecognizer {
    [_scannerSession snap];
}

- (void)dismissAction {
    [_scannerSession stopCapture];
    
    // This is to make sure any pending API search is cancelled
    [_scannerSession cancel];
    //[self dismissModalViewControllerAnimated:YES];
}

#pragma mark - MSScannerSessionDelegate

#if MS_SDK_REQUIREMENTS

- (void)session:(MSScannerSession *)session didScan:(MSResult *)result {
    // Notify the overlay
    // --
    if (result != nil) {
        // We choose to notify only if a *new* result has been found
        if (![_result isEqualToResult:result]) {
            _result = [result copy];
            
            // This is to prevent the scanner to keep scanning while a result
            // is shown on the overlay side (see `resume` method below)
            [_scannerSession pause];
            
            // Make sure this happens into the *main* thread
            CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
                
                [self movieResult:result];
            });
        }
    }
}

- (void)session:(MSScannerSession *)scanner failedToScan:(NSError *)error {
    MSDLog(@" [MOODSTOCKS SDK] SCAN ERROR: %@", MSErrMsg([error code]));
}

- (void)scannerWillSearch:(MSScanner *)scanner {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void)scanner:(MSScanner *)scanner didSearchWithResult:(MSResult *)result {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    if (result != nil) {
        [_scannerSession pause];
        
        [[[UIAlertView alloc] initWithTitle:@"Scanne" message:@"Find !" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
    else {
        // Feel free to choose the proper UI component used to warn the user
        // that the API search could not found a match
        [[[UIAlertView alloc] initWithTitle:@"No match found"
                                     message:nil
                                    delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil] show];
    }
}

- (void)scanner:(MSScanner *)scanner failedToSearchWithError:(NSError *)error {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    ms_errcode ecode = [error code];
    // NOTE: ignore negative error codes (e.g. -1 when the request has been cancelled)
    if (ecode >= 0) {
        NSString *errStr = MSErrMsg(ecode);
        
        MSDLog(@" [MOODSTOCKS SDK] FAILED TO SEARCH WITH ERROR: %@", errStr);
        
        // Here you may want to inform the user that an error occurred
        // Fee free to adapt to your needs (wording, display policy, etc)
        switch (ecode) {
            case MS_NOCONN:
                errStr = @"No Internet connection.";
                break;
                
            case MS_TIMEOUT:
                errStr = @"The request timed out.";
                break;
                
            default:
                errStr = [NSString stringWithFormat:@"An error occurred (code = %d).", ecode];
                break;
        }
        
        // Feel free to choose the proper UI component to warn the user that an error occurred
        [[[UIAlertView alloc] initWithTitle:@"Search error"
                                     message:errStr
                                    delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil] show];
    }
}
#endif



#pragma mark - Public

- (void)resume {
    _result = nil;
    [_scannerSession resume];
}

- (void)movieResult:(MSResult*)result{
    self.movie_base64 =  result.getValue;
    
    MovieViewController *movieView = [[MovieViewController alloc] initWithMovieURLBase64:self.movie_base64];
    [self.navigationController pushViewController:movieView animated:YES];
}

- (void)actionWatchlist{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)actionMovie{
    
    if([self.movie_base64 length] == 0){
        return;
    }

    MovieViewController *movieView = [[MovieViewController alloc] initWithMovieURLBase64:self.movie_base64];
    [self.navigationController pushViewController:movieView animated:YES];
}

@end
