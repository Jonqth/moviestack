//
//  ScannerViewController.h
//  lemobile
//
//  Created by Aymeric on 19/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZViewController.h"

#import "MSScannerSession.h"

@interface ScannerViewController : FZViewController<
#if MS_SDK_REQUIREMENTS
MSScannerSessionDelegate
#endif
>
{
    MSScannerSession *_scannerSession;
    MSResult *_result; // previous result
    UIView *_videoPreview;
}

@property (strong, nonatomic) NSString *movie_base64;
/**
 * Flush the last recognized result (if any) and start scanning again
 */
- (void)resume;


@end
