//
//  SigninViewController.h
//  lemobile
//
//  Created by Aymeric Gallissot on 18/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "FZViewController.h"

@interface SigninViewController : FZViewController

@property (strong, nonatomic) UIActivityIndicatorView *activity;

@end
