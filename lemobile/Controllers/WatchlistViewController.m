//
//  WatchlistViewController.m
//  lemobile
//
//  Created by Aymeric Gallissot on 18/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "WatchlistViewController.h"

#import "ScannerViewController.h"
#import "MovieViewController.h"

#import "FZUser.h"

#import "Movie.h"


@interface WatchlistViewController ()

@end

@implementation WatchlistViewController

- (id)init{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithRed:(30/255.f) green:(30/255.f) blue:(30/255.f) alpha:1]];
    
    self.navigationItem.rightBarButtonItem = [self buttonSettings];
    [self.navigationItem setHidesBackButton:YES];
    
    
    int tableViewSizeHeight = 416;
    if(sharedAppDelegate.mainScreeniPhone5){
        tableViewSizeHeight += 88;
    }
    
    // TableView
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 1, 320, tableViewSizeHeight)];
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    [self.tableView setSeparatorColor:[UIColor colorWithRed:(24/255.f) green:(24/255.f) blue:(24/255.f) alpha:1]];
    [self.view addSubview:self.tableView];
    
    // TableView RefreshControl
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(actionScanner)];
    swipe.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipe];
    
    
    [self loadData];
	
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}


- (void)viewDidAppear:(BOOL)animated{
    // Do any additional setup after loading the view.
    
    NSString *fbid = [FZUser sharedUser].fbid;
    NSString *fbtoken = [FZUser sharedUser].fbtoken;
    
    
    NSLog(@"%@", fbid);
    NSLog(@"%@", fbtoken);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadData{
    RKObjectMapping *movieMapping = [RKObjectMapping mappingForClass:[Movie class]];
    [movieMapping addAttributeMappingsFromDictionary:@{@"id":@"movie_id", @"title":@"title",@"image":@"image",@"trailer":@"trailer",@"genre":@"genre",@"releasedate":@"releasedate",@"runtime":@"runtime",@"rating":@"rating"}];
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:movieMapping pathPattern:nil keyPath:@"data.movies" statusCodes:nil];
    NSString *test = [NSString stringWithFormat:@"http://allocine.eu01.aws.af.cm/watchlist?fb_id=%@", [FZUser sharedUser].fbid];
    NSLog(@"%@", test);
    NSURL *url = [NSURL URLWithString:test];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
    [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
        NSLog(@"Data result: %@", [result array]);
        self.data = [result array];
        [self.tableView reloadData];
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        // The `description` method of the class the error is mapped to is used to construct the value of the localizedDescription
        NSLog(@"Loaded this error: %@", [error localizedDescription]);
    }];
    [operation start];
}

- (void)refreshData{
    [self.refreshControl endRefreshing];
}

- (UIImage*) imageScale:(UIImage *)image andScaleToHeight:(float)s_height andWidth:(float)s_width {
    
    float oldHeight = 0.0, oldWidth = 0.0,scaleFactor = 0.0,newHeight = 0.0,newWidth = 0.0;
    
    if(image.size.height >= image.size.width){
        // à la française
        oldHeight = image.size.height;
        scaleFactor = s_height / oldHeight;
        
        newHeight = image.size.height * scaleFactor;
        newWidth = image.size.width * scaleFactor;
    }else {
        // à l'italienne
        oldWidth = image.size.width;
        scaleFactor = s_width / oldWidth;
        
        newHeight = image.size.height * scaleFactor;
        newWidth = image.size.width * scaleFactor;
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 83.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 43;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.data.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 43)];
    [header setBackgroundColor:sharedAppDelegate.colorTitle];
    
    UIImage *headerImg = [UIImage imageNamed:@"ms_swipeindic.png"];
    UIImageView *headerView = [[UIImageView alloc] initWithFrame:CGRectMake(header.frame.size.width/2-(headerImg.size.width/2), 16, headerImg.size.width, headerImg.size.height)];
    [headerView setImage:headerImg];
    [header addSubview:headerView];
    
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        UIImage *accesoImg = [UIImage imageNamed:@"ms_accessory.png"];
        UIImageView *accessoView = [[UIImageView alloc] initWithImage:accesoImg];
        [cell setAccessoryView:accessoView];
    }
    
    
    Movie *movie = [self.data objectAtIndex:indexPath.row];
    
    // Cell selection
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    UIImage *cellBgView = [UIImage imageNamed:@"ms_cellback.png"];
    UIImageView *cellbg = [[UIImageView alloc] initWithImage:cellBgView];
    
    [cell setBackgroundView:cellbg];
    
    
    // Image
    UIImage *bgImage = [UIImage imageNamed:@"ms_imgback.png"];
    UIImageView *contentImagecontain = [[UIImageView alloc] initWithFrame:CGRectMake(15, 7, bgImage.size.width, bgImage.size.height)];
    [contentImagecontain setImage:bgImage];
    
    UIImageView *poster = [[UIImageView alloc] init];
    // Getting the image
    NSURL *imageUrl = [NSURL URLWithString:movie.image];
    [[SDWebImageManager sharedManager] downloadWithURL:imageUrl options:0
                                              progress:^(NSUInteger receivedSize, long long expectedSize) {
                                                  NSLog(@"loading");
                                              } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
                                                  image = [self imageScale:image andScaleToHeight:(bgImage.size.height-8) andWidth:(bgImage.size.width)-8];
                                                  [poster setImage:image];
                                                  [poster setFrame:CGRectMake(12, 5, image.size.width, image.size.height)];
                                              }];
    [contentImagecontain addSubview:poster];
    
    [cell addSubview:contentImagecontain];
    
    // Title
    UILabel *movieTitle = [[UILabel alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+9, 10, 160, 17)];
    [movieTitle setBackgroundColor:[UIColor clearColor]];
    [movieTitle setFont:sharedAppDelegate.fontTitle];
    [movieTitle setTextColor:sharedAppDelegate.colorTitle];
    [movieTitle setText:movie.title];
    
    movieTitle.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieTitle.layer.shadowOpacity = 0.1;
    movieTitle.layer.shadowRadius = 0.0;
    movieTitle.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [cell addSubview:movieTitle];
    
    // Info
    UILabel *movieInfo = [[UILabel alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+10,
                                                                   (movieTitle.frame.origin.y+movieTitle.frame.size.height), 160, 15)];
    [movieInfo setBackgroundColor:[UIColor clearColor]];
    [movieInfo setFont:sharedAppDelegate.fontInfo];
    [movieInfo setTextColor:sharedAppDelegate.colorInfo];
    [movieInfo setText:movie.genre];
    
    movieInfo.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieInfo.layer.shadowOpacity = 0.1;
    movieInfo.layer.shadowRadius = 0.0;
    movieInfo.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [cell addSubview:movieInfo];
    
    // Date
    UIImage *arrow = [UIImage imageNamed:@"ms_horaires.png"];
    UIImageView *arrowView =[[UIImageView alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+10,
                                                                          (movieInfo.frame.origin.y+movieInfo.frame.size.height)+3, arrow.size.width, arrow.size.height)];
    [arrowView setImage:arrow];
    [cell addSubview:arrowView];
    
    UILabel *movieDate = [[UILabel alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+23,
                                                                   (movieInfo.frame.origin.y+movieInfo.frame.size.height)+3, 160, 10)];
    [movieDate setBackgroundColor:[UIColor clearColor]];
    [movieDate setFont:sharedAppDelegate.fontInfo];
    [movieDate setTextColor:sharedAppDelegate.colorTitle];
    [movieDate setText:movie.releasedate];
    
    movieDate.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieDate.layer.shadowOpacity = 0.1;
    movieDate.layer.shadowRadius = 0.0;
    movieDate.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [cell addSubview:movieDate];
    
    // Durée
    UIImage *clock = [UIImage imageNamed:@"ms_clock.png"];
    UIImageView *clockView =[[UIImageView alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+8,
                                                                          (movieDate.frame.origin.y+movieDate.frame.size.height)+1, clock.size.width, clock.size.height)];
    [clockView setImage:clock];
    [cell addSubview:clockView];
    
    UILabel *movieTime = [[UILabel alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+23,
                                                                   (movieDate.frame.origin.y+movieDate.frame.size.height)+3, 160, 10)];
    [movieTime setBackgroundColor:[UIColor clearColor]];
    [movieTime setFont:sharedAppDelegate.fontInfo];
    [movieTime setTextColor:sharedAppDelegate.colorTitle];
    [movieTime setText:[NSString stringWithFormat:@"%d", movie.runtime]];
    
    movieTime.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieTime.layer.shadowOpacity = 0.1;
    movieTime.layer.shadowRadius = 0.0;
    movieTime.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [cell addSubview:movieTime];
    
    // Rate
    UIImage *rate = [UIImage imageNamed:@"ms_rate.png"];
    UIImageView *rateView =[[UIImageView alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(movieTitle.frame.size.width)+20,
                                                                          (movieDate.frame.origin.y+5), rate.size.width, rate.size.height)];
    [rateView setImage:rate];
    
    UILabel *movieRate = [[UILabel alloc] initWithFrame:CGRectMake(7, 7, 20, 10)];
    [movieRate setBackgroundColor:[UIColor clearColor]];
    [movieRate setFont:sharedAppDelegate.fontRate];
    [movieRate setTextColor:sharedAppDelegate.colorInfo];
    [movieRate setText:[NSString stringWithFormat:@"%.1f", movie.rating]];
    
    movieRate.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieRate.layer.shadowOpacity = 0.1;
    movieRate.layer.shadowRadius = 0.0;
    movieRate.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [rateView addSubview:movieRate];
    [cell addSubview:rateView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MovieViewController *toVC = [[MovieViewController alloc] initWithMovie:[self.data objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:toVC animated:YES];
}

- (void)actionScanner{
    NSLog(@"Swipe");
    
    ScannerViewController *scannerView = [[ScannerViewController alloc] init];
    [self.navigationController pushViewController:scannerView animated:YES];
}

@end
