//
//  MovieViewController.m
//  lemobile
//
//  Created by Aymeric on 19/03/13.
//  Copyright (c) 2013 Fuzzze. All rights reserved.
//

#import "MovieViewController.h"

#import "NSData+Base64.h"

#import "FZBase64.h"

#import <RestKit/RestKit.h>

#import "FZUser.h"

#import <SDWebImage/UIImageView+WebCache.h>

@interface MovieViewController ()

@end

@implementation MovieViewController

@synthesize MSLocationManager;
@synthesize MSMapView;


- (id)initWithMovie:(Movie *)movie
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.movie = movie;
        
    }
    return self;
}

- (id)initWithMovieURLBase64:(NSString *)movieBase64;
{
    self = [super init];
    if (self) {
        // Custom initialization
        self.movie_base64 = movieBase64;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (UIImage*) imageScale:(UIImage *)image andScaleToHeight:(float)s_height andWidth:(float)s_width {
    
    float oldHeight = 0.0, oldWidth = 0.0,scaleFactor = 0.0,newHeight = 0.0,newWidth = 0.0;
    
    if(image.size.height >= image.size.width){
        // à la française
        oldHeight = image.size.height;
        scaleFactor = s_height / oldHeight;
        
        newHeight = image.size.height * scaleFactor;
        newWidth = image.size.width * scaleFactor;
    }else {
        // à l'italienne
        oldWidth = image.size.width;
        scaleFactor = s_width / oldWidth;
        
        newHeight = image.size.height * scaleFactor;
        newWidth = image.size.width * scaleFactor;
    }
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [self buttonBack];
    self.navigationItem.rightBarButtonItem = [self buttonAddWatchlist];
    
        
    // map
    self.MSLocationManager = [[CLLocationManager alloc] init];
    [self.MSLocationManager setDelegate:self];
    [self.MSLocationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [self.MSLocationManager setPausesLocationUpdatesAutomatically:YES];
    [self.MSLocationManager startUpdatingLocation];
    [self.MSLocationManager startUpdatingHeading];
    
    // Map Handling
    self.MSMapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 138)];
    [self.MSMapView setDelegate:self];
    [self.MSMapView setMapType:MKMapTypeStandard];
    [self.MSMapView setUserTrackingMode:MKUserTrackingModeFollow];
    
    [self.view addSubview:MSMapView];
    
    
    // Main
    UIImage *bgMain = [UIImage imageNamed:@"ms_mainback.png"];
    UIImageView *mainView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.MSMapView.frame.origin.y+(self.MSMapView.frame.size.height+1),
                                                                          bgMain.size.width, bgMain.size.height)];
    
    [mainView setImage:bgMain];
    
    // Image
    UIImage *bgImage = [UIImage imageNamed:@"ms_imgback.png"];
    UIImageView *contentImagecontain = [[UIImageView alloc] initWithFrame:CGRectMake(15, 7, bgImage.size.width, bgImage.size.height)];
    UIImageView *poster = [[UIImageView alloc] init];
    // Getting the image
    NSURL *imageUrl = [NSURL URLWithString:self.movie.image];
    [[SDWebImageManager sharedManager] downloadWithURL:imageUrl options:0
                                              progress:^(NSUInteger receivedSize, long long expectedSize) {
                                                  NSLog(@"loading");
                                              } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
                                                  image = [self imageScale:image andScaleToHeight:(bgImage.size.height-8) andWidth:(bgImage.size.width)-8];
                                                  [poster setImage:image];
                                                  [poster setFrame:CGRectMake(12, 5, image.size.width, image.size.height)];
                                              }];
    [contentImagecontain addSubview:poster];
    [contentImagecontain setImage:bgImage];
    [mainView addSubview:contentImagecontain];
    
    
    // Title
    UILabel *movieTitle = [[UILabel alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+9, 10, 160, 17)];
    [movieTitle setBackgroundColor:[UIColor clearColor]];
    [movieTitle setFont:sharedAppDelegate.fontTitle];
    [movieTitle setTextColor:sharedAppDelegate.colorTitle];
    [movieTitle setText:self.movie.title];
    
    movieTitle.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieTitle.layer.shadowOpacity = 0.1;
    movieTitle.layer.shadowRadius = 0.0;
    movieTitle.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [mainView addSubview:movieTitle];
    
    // Info
    UILabel *movieInfo = [[UILabel alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+10,
                                                                   (movieTitle.frame.origin.y+movieTitle.frame.size.height), 160, 15)];
    [movieInfo setBackgroundColor:[UIColor clearColor]];
    [movieInfo setFont:sharedAppDelegate.fontInfo];
    [movieInfo setTextColor:sharedAppDelegate.colorInfo];
    [movieInfo setText:self.movie.genre];
    
    movieInfo.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieInfo.layer.shadowOpacity = 0.1;
    movieInfo.layer.shadowRadius = 0.0;
    movieInfo.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [mainView addSubview:movieInfo];
    
    // Date
    UIImage *arrow = [UIImage imageNamed:@"ms_horaires.png"];
    UIImageView *arrowView =[[UIImageView alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+10,
                                                                          (movieInfo.frame.origin.y+movieInfo.frame.size.height)+3, arrow.size.width, arrow.size.height)];
    [arrowView setImage:arrow];
    [mainView addSubview:arrowView];
    
    UILabel *movieDate = [[UILabel alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+23,
                                                                   (movieInfo.frame.origin.y+movieInfo.frame.size.height)+3, 160, 10)];
    [movieDate setBackgroundColor:[UIColor clearColor]];
    [movieDate setFont:sharedAppDelegate.fontInfo];
    [movieDate setTextColor:sharedAppDelegate.colorTitle];
    [movieDate setText:self.movie.releasedate];
    
    movieDate.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieDate.layer.shadowOpacity = 0.1;
    movieDate.layer.shadowRadius = 0.0;
    movieDate.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [mainView addSubview:movieDate];
    
    // Durée
    UIImage *clock = [UIImage imageNamed:@"ms_clock.png"];
    UIImageView *clockView =[[UIImageView alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+8,
                                                                          (movieDate.frame.origin.y+movieDate.frame.size.height)+1, clock.size.width, clock.size.height)];
    [clockView setImage:clock];
    [mainView addSubview:clockView];
    
    UILabel *movieTime = [[UILabel alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(contentImagecontain.frame.size.width)+23,
                                                                   (movieDate.frame.origin.y+movieDate.frame.size.height)+3, 160, 10)];
    [movieTime setBackgroundColor:[UIColor clearColor]];
    [movieTime setFont:sharedAppDelegate.fontInfo];
    [movieTime setTextColor:sharedAppDelegate.colorTitle];
    [movieTime setText:[NSString stringWithFormat:@"%d", self.movie.runtime]];
    
    movieTime.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieTime.layer.shadowOpacity = 0.1;
    movieTime.layer.shadowRadius = 0.0;
    movieTime.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [mainView addSubview:movieTime];
    
    // Rate
    UIImage *rate = [UIImage imageNamed:@"ms_rate.png"];
    UIImageView *rateView =[[UIImageView alloc] initWithFrame:CGRectMake(contentImagecontain.frame.origin.x+(movieTitle.frame.size.width)+80,
                                                                         (movieDate.frame.origin.y+5), rate.size.width, rate.size.height)];
    [rateView setImage:rate];
    
    UILabel *movieRate = [[UILabel alloc] initWithFrame:CGRectMake(7, 7, 20, 10)];
    [movieRate setBackgroundColor:[UIColor clearColor]];
    [movieRate setFont:sharedAppDelegate.fontRate];
    [movieRate setTextColor:sharedAppDelegate.colorInfo];
    [movieRate setText:@"3,5"];
    
    movieRate.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieRate.layer.shadowOpacity = 0.1;
    movieRate.layer.shadowRadius = 0.0;
    movieRate.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [rateView addSubview:movieRate];
    [mainView addSubview:rateView];
    
    // BA
    UIImage *see = [UIImage imageNamed:@"ms_seebutton.png"];
    UIButton *seebutton = [[UIButton alloc] initWithFrame:CGRectMake(mainView.frame.size.width/2-(see.size.width/2), mainView.frame.size.height-see.size.height-7,
                                                                     see.size.width, see.size.height)];
    [seebutton setBackgroundImage:see forState:UIControlStateNormal];
    [seebutton setBackgroundColor:[UIColor clearColor]];
    [seebutton addTarget:self action:@selector(play) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:seebutton];
    
    [self.view addSubview:mainView];
    
    // table
    
    UITableView *timeTable = [[UITableView alloc] initWithFrame:CGRectMake(mainView.frame.origin.x, mainView.frame.origin.y+mainView.frame.size.height
                                                                           , self.view.frame.size.width, self.view.frame.size.height)];
    [timeTable setBackgroundColor:[UIColor clearColor]];
    [timeTable setDataSource:self];
    [timeTable setDelegate:self];
    [timeTable setSeparatorColor:sharedAppDelegate.colorTitle];
    [self.view addSubview:timeTable];
    
    if([self.movie_base64 length] > 0){
        
        NSData *data = [FZBase64 decodeBase64WithString:self.movie_base64];
        NSString* json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSData *jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        
        json = nil;
        if (!dict) {
            dict = [NSDictionary dictionary];
        }
        
        //[[[UIAlertView alloc] initWithTitle:[dict objectForKey:@"name"] message:[dict objectForKey:@"id"] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil] show];
        
        RKObjectMapping *movieMapping = [RKObjectMapping mappingForClass:[Movie class]];
        [movieMapping addAttributeMappingsFromDictionary:@{@"id":@"movie_id", @"title":@"title",@"image":@"image",@"trailer":@"trailer",@"genre":@"genre",@"releasedate":@"releasedate",@"runtime":@"runtime",@"rating":@"rating"}];
        
        RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:movieMapping pathPattern:nil keyPath:@"data.movie" statusCodes:nil];
        NSString *test = [NSString stringWithFormat:@"http://allocine.eu01.aws.af.cm/movies/%@", [dict objectForKey:@"id"]];
                          NSLog(@"%@", test);
                          NSURL *url = [NSURL URLWithString:test];
                          NSURLRequest *request = [NSURLRequest requestWithURL:url];
                          RKObjectRequestOperation *operation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor]];
                          [operation setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *result) {
            NSLog(@"Data result: %@", [result array]);
                              
            NSArray *data = [result array];
            Movie *movie = [data objectAtIndex:0];
                              
                              
                              [movieTitle setText:movie.title];
                              [movieInfo setText:movie.genre];
                              [movieDate setText:movie.releasedate];
                              [movieTime setText:[NSString stringWithFormat:@"%d", movie.runtime]];
                              [movieRate setText:[NSString stringWithFormat:@"%.1f", movie.rating]];
                              
                              NSURL *imageUrl = [NSURL URLWithString:movie.image];
                              [[SDWebImageManager sharedManager] downloadWithURL:imageUrl options:0
                                                                        progress:^(NSUInteger receivedSize, long long expectedSize) {
                                                                            NSLog(@"loading");
                                                                        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
                                                                            image = [self imageScale:image andScaleToHeight:(bgImage.size.height-8) andWidth:(bgImage.size.width)-8];
                                                                            [poster setImage:image];
                                                                            [poster setFrame:CGRectMake(12, 5, image.size.width, image.size.height)];
                                                                        }];
                              
                              
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            // The `description` method of the class the error is mapped to is used to construct the value of the localizedDescription
            NSLog(@"Loaded this error: %@", [error localizedDescription]);
        }];
                          [operation start];
                          
        
        
        
                          // [FZUser sharedUser].fbid]
                          }

}

- (void) play {
    NSLog(@"play");
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 83.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 22;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 6;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 43)];
    [header setBackgroundColor:sharedAppDelegate.colorTitle];
    
    UIImage *headerImg = [UIImage imageNamed:@"ms_seances.png"];
    UIImageView *headerView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 6, headerImg.size.width, headerImg.size.height)];
    [headerView setImage:headerImg];
    [header addSubview:headerView];
    
    return header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        UIImage *accesoImg = [UIImage imageNamed:@"ms_accessory.png"];
        UIImageView *accessoView = [[UIImageView alloc] initWithImage:accesoImg];
        [cell setAccessoryView:accessoView];
    }
    
    
    // Movie *movie = [self.data objectAtIndex:indexPath.row];
    
    // Cell selection
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setBackgroundColor:[UIColor clearColor]];
    
    UIImage *cellBgView = [UIImage imageNamed:@"ms_cellback.png"];
    UIImageView *cellbg = [[UIImageView alloc] initWithImage:cellBgView];
    
    [cell setBackgroundView:cellbg];
    
    // Title
    UILabel *movieTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 160, 17)];
    [movieTitle setBackgroundColor:[UIColor clearColor]];
    [movieTitle setFont:sharedAppDelegate.fontTitle];
    [movieTitle setTextColor:sharedAppDelegate.colorTitle];
    [movieTitle setText:@"MK2"];
    
    movieTitle.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieTitle.layer.shadowOpacity = 0.1;
    movieTitle.layer.shadowRadius = 0.0;
    movieTitle.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [cell addSubview:movieTitle];
    
    // Info
    UILabel *movieInfo = [[UILabel alloc] initWithFrame:CGRectMake(10,
                                                                   (movieTitle.frame.origin.y+movieTitle.frame.size.height), 160, 15)];
    [movieInfo setBackgroundColor:[UIColor clearColor]];
    [movieInfo setFont:sharedAppDelegate.fontInfo];
    [movieInfo setTextColor:sharedAppDelegate.colorInfo];
    [movieInfo setText:@"19:45"];
    
    movieInfo.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieInfo.layer.shadowOpacity = 0.1;
    movieInfo.layer.shadowRadius = 0.0;
    movieInfo.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [cell addSubview:movieInfo];
    
    // Date
    UIImage *arrow = [UIImage imageNamed:@"ms_horairesfat.png"];
    UIImageView *arrowView =[[UIImageView alloc] initWithFrame:CGRectMake(10,
                                                                          (movieInfo.frame.origin.y+movieInfo.frame.size.height)+3, arrow.size.width, arrow.size.height)];
    [arrowView setImage:arrow];
    [cell addSubview:arrowView];
    
    UILabel *movieDate = [[UILabel alloc] initWithFrame:CGRectMake(23,
                                                                   (movieInfo.frame.origin.y+movieInfo.frame.size.height)+3, 160, 10)];
    [movieDate setBackgroundColor:[UIColor clearColor]];
    [movieDate setFont:sharedAppDelegate.fontRate];
    [movieDate setTextColor:sharedAppDelegate.colorTitle];
    [movieDate setText:@"23 Février 2013"];
    
    movieDate.layer.shadowColor = [[UIColor whiteColor] CGColor];
    movieDate.layer.shadowOpacity = 0.1;
    movieDate.layer.shadowRadius = 0.0;
    movieDate.layer.shadowOffset = CGSizeMake(.0f, 1.0f);
    
    [cell addSubview:movieDate];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //MovieViewController *toVC = [[MovieViewController alloc] initWithMovie:[self.data objectAtIndex:indexPath.row]];
    //[self.navigationController pushViewController:toVC animated:YES];
}

- (void)actionAddWatchlist{
    NSLog(@"test");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
